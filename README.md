![Findmore Academy - Formação GitLab CI/CD](./formacao.png "Formação GitLab CI/CD")

Este é um repositório base com código de uma aplicação React Hello World com modificações simples, que será usado como conteúdo inicial para os trabalhos com pipelines no GitLab CI/CD.

Também há um código simples de Terraform que será usado para ler um output criado no repositório que gerencia a infraestrutura na Azure.

---
---

## Documentação GitLab

- [.gitlab-ci.yml Keyword Reference](https://docs.gitlab.com/ee/ci/yaml/)
- [GitLab Predefined Variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [GitLab CI/CD Variable Precedence](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence)
- [Difference Between Cache and Artifacts](https://docs.gitlab.com/ee/ci/caching/)
- [Choose When to Run Jobs](https://docs.gitlab.com/ee/ci/jobs/job_control.html)