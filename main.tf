terraform {
  required_version = ">= 1.3.0"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.53.0"
    }
  }
}

provider "azurerm" {
  features {}
}

data "terraform_remote_state" "storage_account" {
  backend = "azurerm"
  config = {
    resource_group_name  = "rg-terraform-state-outcloud"
    storage_account_name = "saterraformstateoutcloud"
    container_name       = "remote-state"
    key                  = "${var.student_name}-${var.environment}/terraform.tfstate"
  }
}